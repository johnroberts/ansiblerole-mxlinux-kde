# Summary
Ansible role for managing MX Linux 23 KDE

# Platforms
MX 23 KDE (prolly works in MX broadly)

# Customization Options
- Conky (for desktop info widget)
- Yakuake (for ZSH + theme)

# Usage
View and override vars `defaults/main.yml` to change behavior

# Dev Notes
Uses `kwriteconfig5` and `kreadconfig5` (from `kde-cli-tools` apt package) to manage KDE config files (which are INI-style key-value pairs)

## Config files for UI/behavior tweaks
virtual desktop shortcuts: `/.config/kglobalshortcutsrc` -> [kwin] section
number of virtual desktops, layout: `~/.config/kwinrc` -> Desktops
