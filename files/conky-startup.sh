#!/bin/sh

sleep 20s
killall -u $(id -nu) conky 2>/dev/null
cd "$HOME/.conky/MX-MyConky"
conky -c "$HOME/.conky/MX-MyConky/MySysInfoConky" &
exit 0
